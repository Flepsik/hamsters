package flepsik.github.com.hamsters.repository.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.util.SortedList;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import flepsik.github.com.hamsters.common.LoadingCallback;
import flepsik.github.com.hamsters.common.utils.HamsterUtils;
import flepsik.github.com.hamsters.models.Hamster;

public class HamsterDatabaseRepository implements DatabaseRepository {
    private static final int STATE_SUCCESS = 100;
    private static final int STATE_FAIL = 101;

    private final HamsterDatabaseHelper databaseHelper;

    public HamsterDatabaseRepository(Context context) {
        this.databaseHelper = new HamsterDatabaseHelper(context);
    }

    @Override
    public void getHamsters(LoadingCallback<List<Hamster>> callback) {
        final List<Hamster> result = new ArrayList<>();
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case STATE_SUCCESS:
                        callback.onSuccess(result);
                        break;
                    case STATE_FAIL:
                        callback.onError(new Exception("Failed to download from database"));
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        };

        new Thread() {
            @Override
            public void run() {
                SQLiteDatabase database = databaseHelper.getReadableDatabase();
                Cursor cursor = database.query(HamsterDatabaseHelper.TABLE_NAME,
                        HamsterDatabaseHelper.DEFAULT_PROJECTION,
                        null, null, null, null,
                        HamsterDatabaseHelper.DEFAULT_SORT_ORDER
                );
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            String name = cursor.getString(cursor.getColumnIndex(HamsterDatabaseHelper.KEY_NAME));
                            String description = cursor.getString(cursor.getColumnIndex(HamsterDatabaseHelper.KEY_DESCRIPTION));
                            String url = cursor.getString(cursor.getColumnIndex(HamsterDatabaseHelper.KEY_URL_IMAGE));
                            boolean important = cursor.getInt(cursor.getColumnIndex(HamsterDatabaseHelper.KEY_IMPORTANT)) == 1;
                            result.add(new Hamster(name, description, url, important));
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                    handler.sendEmptyMessage(STATE_SUCCESS);
                } else {
                    handler.sendEmptyMessage(STATE_FAIL);
                }
            }
        }.run();
    }

    @Override
    public void replaceHamsters(List<Hamster> hamsterList) {
        if (hamsterList != null && hamsterList.size() > 0) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    SQLiteDatabase database = databaseHelper.getWritableDatabase();
                    database.delete(
                            HamsterDatabaseHelper.TABLE_NAME,
                            null,
                            null
                    );
                    for (Hamster hamster : hamsterList) {
                        database.insertWithOnConflict(
                                HamsterDatabaseHelper.TABLE_NAME,
                                null,
                                HamsterUtils.toContentValues(hamster),
                                SQLiteDatabase.CONFLICT_REPLACE
                        );
                    }
                    return null;
                }
            };
        }
    }
}
