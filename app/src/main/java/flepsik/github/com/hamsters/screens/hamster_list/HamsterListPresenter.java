package flepsik.github.com.hamsters.screens.hamster_list;

import flepsik.github.com.hamsters.common.mvp.BasePresenter;
import flepsik.github.com.hamsters.common.mvp.BindView;
import flepsik.github.com.hamsters.common.mvp.Presenter;

public interface HamsterListPresenter extends Presenter<HamsterListView> {
    void onItemClicked(int index);

    void onRefresh();

    void searchRequested(String query);

    void searchOpened();

    void searchClosed();

    boolean isSearchOpened();

    String getSearchQuery();
}
