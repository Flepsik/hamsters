package flepsik.github.com.hamsters.models;

public class QueryParameters {
    private String name;
    private String in;
    private String type;
    private boolean required;
    private String description;

    public QueryParameters(String name, String in, String type, boolean required, String description) {
        this.name = name;
        this.in = in;
        this.type = type;
        this.required = required;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIn() {
        return in;
    }

    public void setIn(String in) {
        this.in = in;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
