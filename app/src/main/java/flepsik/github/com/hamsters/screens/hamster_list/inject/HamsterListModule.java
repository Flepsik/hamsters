package flepsik.github.com.hamsters.screens.hamster_list.inject;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import flepsik.github.com.hamsters.common.InjectConstants;
import flepsik.github.com.hamsters.common.annotations.ActivityScope;
import flepsik.github.com.hamsters.common.mvp.PresenterCache;
import flepsik.github.com.hamsters.repository.database.DatabaseRepository;
import flepsik.github.com.hamsters.repository.network.NetworkRepository;
import flepsik.github.com.hamsters.screens.hamster_list.HamsterListModel;
import flepsik.github.com.hamsters.screens.hamster_list.HamsterListPresenter;
import flepsik.github.com.hamsters.screens.hamster_list.impl.DefaultHamsterListModel;
import flepsik.github.com.hamsters.screens.hamster_list.impl.DefaultHamsterListPresenter;
import retrofit2.Retrofit;

@Module
public class HamsterListModule {
    private final String tag;

    public HamsterListModule(String tag) {
        this.tag = tag;
    }

    @ActivityScope
    @Provides
    public HamsterListPresenter providePresenter(HamsterListModel model) {
        return PresenterCache.getInstance().getPresenter(tag,
                () -> new DefaultHamsterListPresenter(model),
                PresenterCache.CacheType.TEMPORARY);
    }

    @ActivityScope
    @Provides
    public HamsterListModel provideModel(NetworkRepository networkRepository,
                                         DatabaseRepository databaseRepository) {
        return new DefaultHamsterListModel(networkRepository, databaseRepository);
    }
}
