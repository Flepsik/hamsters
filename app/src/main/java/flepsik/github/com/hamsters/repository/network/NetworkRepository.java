package flepsik.github.com.hamsters.repository.network;

import java.util.List;

import flepsik.github.com.hamsters.common.LoadingCallback;
import flepsik.github.com.hamsters.models.Hamster;

public interface NetworkRepository {
    void loadHamsters(LoadingCallback<List<Hamster>> callback);
}
