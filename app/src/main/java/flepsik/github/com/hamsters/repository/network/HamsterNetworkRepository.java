package flepsik.github.com.hamsters.repository.network;

import android.accounts.NetworkErrorException;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.List;

import flepsik.github.com.hamsters.common.LoadingCallback;
import flepsik.github.com.hamsters.models.Hamster;
import flepsik.github.com.hamsters.repository.network.api.HamsterApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HamsterNetworkRepository implements NetworkRepository {
    private final Retrofit retrofit;
    private String appParameters; // я очень хотел сделать get запрос с body, однако, не получилось. Решил не уделять этому значительное внимание

    public HamsterNetworkRepository(Retrofit retrofit, String appParameters) {
        this.retrofit = retrofit;
        this.appParameters = appParameters;
    }

    @Override
    public void loadHamsters(LoadingCallback<List<Hamster>> callback) {
        Call<List<Hamster>> hamsters = retrofit.create(HamsterApi.class).getHamsters();
        hamsters.enqueue(new Callback<List<Hamster>>() {
            @Override
            public void onResponse(@NonNull Call<List<Hamster>> call, @NonNull Response<List<Hamster>> response) {
                if (callback != null) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                    } else {
                        try {
                            callback.onError(new NetworkErrorException(response.errorBody().string()));
                        } catch (IOException | NullPointerException e) {
                            e.printStackTrace();
                            callback.onError(new NetworkErrorException());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Hamster>> call, @NonNull Throwable t) {
                callback.onError(t);
            }
        });
    }
}
