package flepsik.github.com.hamsters.screens.hamster_list.inject;

import dagger.Subcomponent;
import flepsik.github.com.hamsters.common.annotations.ActivityScope;
import flepsik.github.com.hamsters.screens.hamster_list.impl.HamsterListActivity;

@ActivityScope
@Subcomponent(modules = HamsterListModule.class)
public interface HamsterListComponent {
    HamsterListActivity inject(HamsterListActivity activity);
}
