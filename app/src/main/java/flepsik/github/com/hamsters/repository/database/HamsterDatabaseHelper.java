package flepsik.github.com.hamsters.repository.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import flepsik.github.com.hamsters.BuildConfig;

public class HamsterDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "HamsterDatabase";
    private static final int DATABASE_VERSION = (int) BuildConfig.DATABASE_VERSION;

    public static final String TABLE_NAME = "hamster";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_URL_IMAGE = "url_image";
    public static final String KEY_IMPORTANT = "important";
    public static final String[] DEFAULT_PROJECTION = new String[]{
            KEY_ID,
            KEY_NAME,
            KEY_DESCRIPTION,
            KEY_URL_IMAGE,
            KEY_IMPORTANT
    };
    public static final String DEFAULT_SORT_ORDER = KEY_IMPORTANT + " DESC";
    private static final String CREATE_TABLE =
            "create table " + TABLE_NAME + "("
                    + KEY_ID + " integer primary key not null, "
                    + KEY_NAME + " string not null, "
                    + KEY_DESCRIPTION + " string not null, "
                    + KEY_URL_IMAGE + " string, "
                    + KEY_IMPORTANT + " integer default 0);";


    public HamsterDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
