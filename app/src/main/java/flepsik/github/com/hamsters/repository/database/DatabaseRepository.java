package flepsik.github.com.hamsters.repository.database;

import java.util.List;

import flepsik.github.com.hamsters.common.LoadingCallback;
import flepsik.github.com.hamsters.models.Hamster;

public interface DatabaseRepository {
    void getHamsters(LoadingCallback<List<Hamster>> callback);

    void replaceHamsters(List<Hamster> hamsterList);
}
