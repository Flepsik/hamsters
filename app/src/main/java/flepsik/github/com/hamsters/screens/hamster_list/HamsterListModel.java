package flepsik.github.com.hamsters.screens.hamster_list;

import java.util.List;

import flepsik.github.com.hamsters.common.LoadingCallback;
import flepsik.github.com.hamsters.models.Hamster;

public interface HamsterListModel {
    void getHamsters(LoadingCallback<List<Hamster>> callback);
}
