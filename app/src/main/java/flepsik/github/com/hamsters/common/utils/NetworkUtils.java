package flepsik.github.com.hamsters.common.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import flepsik.github.com.hamsters.BuildConfig;
import flepsik.github.com.hamsters.R;
import flepsik.github.com.hamsters.models.QueryParameters;

public class NetworkUtils {
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static QueryParameters[] getAppQueryParameters(Context context) {
        QueryParameters[] queryParameters = new QueryParameters[3];

        queryParameters[0] = new QueryParameters(
                "X-Homo-Client-OS",
                "header",
                "string",
                true,
                "Android " + Build.VERSION.RELEASE);
        queryParameters[1] = new QueryParameters(
                "X-Homo-Client-Version",
                "header",
                "string",
                true,
                context.getString(R.string.app_name) + " " + BuildConfig.VERSION_NAME + "." + BuildConfig.VERSION_CODE);
        queryParameters[2] = new QueryParameters(
                "X-Homo-Client-Model",
                "header",
                "string",
                true,
                Build.MANUFACTURER + " " + Build.PRODUCT);
        return queryParameters;
    }
}