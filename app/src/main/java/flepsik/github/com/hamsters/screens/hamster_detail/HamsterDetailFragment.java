package flepsik.github.com.hamsters.screens.hamster_detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import flepsik.github.com.hamsters.R;
import flepsik.github.com.hamsters.models.Hamster;

public class HamsterDetailFragment extends Fragment {
    private static final String ARG_HAMSTER = "KEY_HAMSTER";
    private static final String ARG_TWO_PANEL = "KEY_TWO_PANEL";

    private Hamster hamster;
    private boolean twoPanel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hamster = getArguments().getParcelable(ARG_HAMSTER);
        if (hamster == null) {
            throw new IllegalStateException("You have to pass hamster to fragment to use it");
        }
        twoPanel = getArguments().getBoolean(ARG_TWO_PANEL);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.hamster_detail, container, false);

        TextView descriptionView = (TextView) rootView.findViewById(R.id.description);
        descriptionView.setText(hamster.getDescription());
        ImageView avatarView = (ImageView) rootView.findViewById(R.id.avatar);
        TextView titleView = (TextView) rootView.findViewById(R.id.title);
        if (twoPanel) {
            Picasso.with(getActivity())
                    .load(hamster.getUrlImage())
                    .error(R.drawable.empty_placeholder_image)
                    .into(avatarView);

            titleView.setText(hamster.getName());
        } else {
            avatarView.setVisibility(View.GONE);
            titleView.setVisibility(View.GONE);
        }

        rootView.findViewById(R.id.share_button).setOnClickListener(view -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, hamster.getName(), hamster.getDescription()));
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        });
        return rootView;
    }

    public static HamsterDetailFragment newInstance(@NonNull final Hamster hamster, boolean twoPanel) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_HAMSTER, hamster);
        args.putBoolean(ARG_TWO_PANEL, twoPanel);
        HamsterDetailFragment fragment = new HamsterDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
