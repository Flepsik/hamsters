package flepsik.github.com.hamsters.screens.hamster_detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import flepsik.github.com.hamsters.*;
import flepsik.github.com.hamsters.models.Hamster;

public class HamsterDetailActivity extends AppCompatActivity {
    private static final String EXTRA_HAMSTER_KEY = "EXTRA_HAMSTER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Hamster hamster = getIntent().getParcelableExtra(EXTRA_HAMSTER_KEY);
        if (hamster != null) {
            setContentView(R.layout.activity_hamster_detail);

            Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
            setSupportActionBar(toolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle(hamster.getName());
            }

            ImageView avatarView = (ImageView) findViewById(R.id.avatar);
            Picasso.with(this).load(hamster.getUrlImage()).into(avatarView);

            if (savedInstanceState == null) {
                Fragment detailFragment = HamsterDetailFragment.newInstance(hamster, false);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.hamster_detail_container, detailFragment)
                        .commit();
            }
        } else {
            Toast.makeText(this.getApplicationContext(), R.string.error_open_hamster_detail, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Context context, Hamster hamster) {
        Intent intent = new Intent(context, HamsterDetailActivity.class);
        intent.putExtra(EXTRA_HAMSTER_KEY, hamster);
        context.startActivity(intent);
    }
}
