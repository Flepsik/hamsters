package flepsik.github.com.hamsters.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Hamster implements Parcelable {
    @SerializedName("title")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("image")
    private String urlImage;
    @SerializedName("pinned")
    private boolean important;

    public Hamster() {
    }

    public Hamster(String name, String description, String urlImage, boolean important) {
        this.name = name;
        this.description = description;
        this.important = important;
        this.urlImage = urlImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeByte(this.important ? (byte) 1 : (byte) 0);
        dest.writeString(this.urlImage);
    }

    protected Hamster(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.important = in.readByte() != 0;
        this.urlImage = in.readString();
    }

    public static final Parcelable.Creator<Hamster> CREATOR = new Parcelable.Creator<Hamster>() {
        @Override
        public Hamster createFromParcel(Parcel source) {
            return new Hamster(source);
        }

        @Override
        public Hamster[] newArray(int size) {
            return new Hamster[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hamster hamster = (Hamster) o;

        if (important != hamster.important) return false;
        if (!name.equals(hamster.name)) return false;
        if (!description.equals(hamster.description)) return false;
        return urlImage != null ? urlImage.equals(hamster.urlImage) : hamster.urlImage == null;

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + (urlImage != null ? urlImage.hashCode() : 0);
        result = 31 * result + (important ? 1 : 0);
        return result;
    }
}
