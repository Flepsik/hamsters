package flepsik.github.com.hamsters;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import flepsik.github.com.hamsters.common.NetworkState;

public class App extends Application {
    private static final String BASE_URL = "http://unrealmojo.com/porn/";

    public static NetworkState sNetworkState;

    private static AppComponent component;
    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
    }

    protected void initialize() {
        component = buildComponent();
        initInternetConnectivityReceiver();
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule(BASE_URL))
                .build();
    }

    private void initInternetConnectivityReceiver() {
        sNetworkState = new NetworkState(NetworkState.getConnectivityStatus(this));
        BroadcastReceiver internetConnectivityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int newStatus = NetworkState.getConnectivityStatus(context);
                if (newStatus != sNetworkState.status) {
                    sNetworkState.status = newStatus;
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(internetConnectivityReceiver, filter);
    }
}
