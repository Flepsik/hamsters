package flepsik.github.com.hamsters.screens.hamster_list.impl;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.List;

import flepsik.github.com.hamsters.App;
import flepsik.github.com.hamsters.R;
import flepsik.github.com.hamsters.common.RecyclerItemClickListener;
import flepsik.github.com.hamsters.common.mvp.PresenterActivity;
import flepsik.github.com.hamsters.models.Hamster;
import flepsik.github.com.hamsters.screens.about.AboutActivity;
import flepsik.github.com.hamsters.screens.hamster_detail.HamsterDetailActivity;
import flepsik.github.com.hamsters.screens.hamster_detail.HamsterDetailFragment;
import flepsik.github.com.hamsters.screens.hamster_list.HamsterListPresenter;
import flepsik.github.com.hamsters.screens.hamster_list.HamsterListView;
import flepsik.github.com.hamsters.screens.hamster_list.inject.HamsterListModule;

public class HamsterListActivity extends PresenterActivity<HamsterListView, HamsterListPresenter> implements HamsterListView {
    private static final int SEARCH_DELAY = 1000;

    private boolean twoPanelMode;
    private View progressView;
    private View emptyPlaceholderView;
    private SwipeRefreshLayout refreshLayout;
    private HamsterListAdapter adapter;
    private Handler searchHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hamster_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        View recyclerView = findViewById(R.id.hamster_list);
        setupRecyclerView((RecyclerView) recyclerView);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        refreshLayout.setOnRefreshListener(() -> {
            presenter.onRefresh();
        });

        progressView = findViewById(R.id.progress_view);
        emptyPlaceholderView = findViewById(R.id.empty_placeholder);

        if (findViewById(R.id.hamster_detail_container) != null) {
            twoPanelMode = true;
        }

        searchHandler = new Handler();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        searchHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void inject() {
        App.getComponent()
                .plus(new HamsterListModule(tag))
                .inject(this);
    }

    @NonNull
    @Override
    public HamsterListView getBindView() {
        return this;
    }

    @Override
    public void setRefreshing(boolean isRefreshing) {
        refreshLayout.setRefreshing(isRefreshing);
    }

    @Override
    public void setProgress(boolean inProgress) {
        progressView.setVisibility(inProgress ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPlaceholder(boolean shouldShow) {
        emptyPlaceholderView.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void openDetails(Hamster hamster) {
        if (twoPanelMode) {
            Fragment detailFragment = HamsterDetailFragment.newInstance(hamster, true);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.hamster_detail_container, detailFragment)
                    .commit();
        } else {
            HamsterDetailActivity.start(this, hamster);
        }
    }

    @Override
    public void showHamsters(List<Hamster> hamsterList) {
        adapter.setItems(hamsterList);
    }

    @Override
    public void selectItem(int index) {
        if (twoPanelMode) {
            adapter.setSelectedIndex(index);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.hamster_list_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        if (presenter.isSearchOpened()) {
            MenuItemCompat.expandActionView(searchItem);
            searchView.setQuery(presenter.getSearchQuery(), false);
            searchView.clearFocus();
        }
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                presenter.searchOpened();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                presenter.searchClosed();
                return true;
            }
        });
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (null != searchManager) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchHandler.removeCallbacksAndMessages(null);
                searchRequested(query);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchHandler.removeCallbacksAndMessages(null);
                if (TextUtils.isEmpty(newText)) {
                    searchRequested(newText);
                    searchHandler.removeCallbacksAndMessages(null);
                } else {
                    searchHandler.postDelayed(() -> searchRequested(newText), SEARCH_DELAY);
                }
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.action_about) {
            startActivity(new Intent(this, AboutActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void searchRequested(String text) {
        presenter.searchRequested(text);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        adapter = new HamsterListAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                presenter.onItemClicked(position);
            }
        }));
    }
}
