package flepsik.github.com.hamsters.common.mvp;

import android.os.Bundle;
import android.support.annotation.NonNull;

public interface Presenter<View extends BindView> {
    void onCreate(Bundle savedInstanceState);

    void updateView();

    void bindView(@NonNull View view);

    void unbindView();

    void saveState(Bundle outState);

    void onDestroy();
}
