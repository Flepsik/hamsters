package flepsik.github.com.hamsters;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import flepsik.github.com.hamsters.common.InjectConstants;
import flepsik.github.com.hamsters.common.utils.NetworkUtils;
import flepsik.github.com.hamsters.models.QueryParameters;
import flepsik.github.com.hamsters.repository.database.DatabaseRepository;
import flepsik.github.com.hamsters.repository.database.HamsterDatabaseRepository;
import flepsik.github.com.hamsters.repository.network.HamsterNetworkRepository;
import flepsik.github.com.hamsters.repository.network.NetworkRepository;
import retrofit2.Retrofit;

@Module
public class AppModule {
    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    @Named(InjectConstants.JSON_APP_PARAMS)
    public String provideAppParamsJson(Context context) {
        Gson gson = new Gson();

        QueryParameters[] queryParameters = NetworkUtils.getAppQueryParameters(context);
        return gson.toJson(queryParameters);
    }

    @Provides
    @Singleton
    public NetworkRepository provideNetworkRepository(Retrofit retrofit,
                                                      @Named(InjectConstants.JSON_APP_PARAMS) String params) {
        return new HamsterNetworkRepository(retrofit, params);
    }

    @Provides
    @Singleton
    public DatabaseRepository provideDatabaseRepository(Context context) {
        return new HamsterDatabaseRepository(context);
    }
}
