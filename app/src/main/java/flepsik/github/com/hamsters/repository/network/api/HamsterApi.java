package flepsik.github.com.hamsters.repository.network.api;

import java.util.List;

import flepsik.github.com.hamsters.models.Hamster;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;

public interface HamsterApi {

    @GET("test3")
    Call<List<Hamster>> getHamsters();
}
