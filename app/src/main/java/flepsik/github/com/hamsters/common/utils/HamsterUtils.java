package flepsik.github.com.hamsters.common.utils;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import flepsik.github.com.hamsters.models.Hamster;
import flepsik.github.com.hamsters.repository.database.HamsterDatabaseHelper;

public class HamsterUtils {
    @NonNull
    public static ContentValues toContentValues(Hamster hamster) {
        ContentValues values = new ContentValues();
        values.put(HamsterDatabaseHelper.KEY_NAME, hamster.getName());
        values.put(HamsterDatabaseHelper.KEY_DESCRIPTION, hamster.getDescription());
        values.put(HamsterDatabaseHelper.KEY_URL_IMAGE, hamster.getUrlImage());
        values.put(HamsterDatabaseHelper.KEY_IMPORTANT, hamster.isImportant() ? 1 : 0);
        return values;
    }
}
