package flepsik.github.com.hamsters.common.mvp;

public interface PresenterFactory<View extends BindView, P extends Presenter<View>> {
    P create();
}
