package flepsik.github.com.hamsters.common.mvp;

import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public class PresenterCache {
    public enum CacheType {NONE, TEMPORARY, PERMANENT}

    private static PresenterCache instance = null;

    private SimpleArrayMap<String, Presenter> permanentCache;
    private Map<String, SoftReference<Presenter>> tempCache;

    private PresenterCache() {
        permanentCache = new SimpleArrayMap<>();
        tempCache = new HashMap<>();
    }

    public static PresenterCache getInstance() {
        if (instance == null) {
            instance = new PresenterCache();
        }
        return instance;
    }

    public final <V extends BindView, T extends Presenter<V>> T getPresenter(String tag,
                                                                             PresenterFactory<V, T> presenterFactory,
                                                                             CacheType type) {
        T presenter = null;
        try {
            //noinspection unchecked
            presenter = (T) findInCache(tag);
        } catch (Exception ignored) {
        }

        if (presenter == null) {
            presenter = presenterFactory.create();
            cachePresenter(tag, presenter, type);
        }
        return presenter;
    }

    @Nullable
    private Presenter findInCache(String tag) {
        Presenter presenter;

        presenter = permanentCache.get(tag);
        if (presenter != null) {
            return presenter;
        }

        SoftReference<Presenter> reference = tempCache.get(tag);
        if (reference != null) {
            presenter = reference.get();
        }

        if (presenter != null) {
            return presenter;
        }

        return null;
    }

    private void cachePresenter(String tag, Presenter presenter, CacheType type) {
        switch (type) {
            case TEMPORARY:
                saveInTempCache(tag, presenter);
                break;
            case PERMANENT:
                saveInPermanentCache(tag, presenter);
                break;
        }
    }

    private void saveInTempCache(String tag, Presenter presenter) {
        tempCache.put(tag, new SoftReference<>(presenter));
    }

    private void saveInPermanentCache(String tag, Presenter presenter) {
        permanentCache.put(tag, presenter);
    }

    /**
     * Remove the presenter associated with the given tag
     *
     * @param tag an unique tag to identify the presenter
     */

    public final void removePresenter(String tag) {
        if (permanentCache != null) {
            permanentCache.remove(tag);
        }

        if (tempCache != null) {
            tempCache.remove(tag);
        }
    }
}