package flepsik.github.com.hamsters;

import javax.inject.Singleton;

import dagger.Component;
import flepsik.github.com.hamsters.screens.hamster_list.inject.HamsterListComponent;
import flepsik.github.com.hamsters.screens.hamster_list.inject.HamsterListModule;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {
    HamsterListComponent plus(HamsterListModule module);
}
