package flepsik.github.com.hamsters.screens.hamster_list.impl;

import android.os.AsyncTask;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import flepsik.github.com.hamsters.common.LoadingCallback;
import flepsik.github.com.hamsters.common.mvp.BasePresenter;
import flepsik.github.com.hamsters.models.Hamster;
import flepsik.github.com.hamsters.screens.hamster_list.HamsterListModel;
import flepsik.github.com.hamsters.screens.hamster_list.HamsterListPresenter;
import flepsik.github.com.hamsters.screens.hamster_list.HamsterListView;

public class DefaultHamsterListPresenter extends BasePresenter<HamsterListView> implements HamsterListPresenter {
    private final HamsterListModel model;
    private List<Hamster> allHamsters = new ArrayList<>();
    private List<Hamster> shownHamsters = new ArrayList<>();
    @LoadingState
    private int state = STATUS_NONE;
    private int selectedItem = -1;
    private String searchQuery = "";
    private boolean isSearching;
    private SearchingAsyncTask searchingAsyncTask;

    public DefaultHamsterListPresenter(HamsterListModel model) {
        this.model = model;
        initialize();
    }

    @Override
    public void updateView() {
        super.updateView();
        HamsterListView view = view();
        if (view != null) {

            view.showHamsters(shownHamsters);
            view.setProgress(state == STATUS_LOADING);
            view.setRefreshing(state == STATUS_REFRESHING);
            view.showPlaceholder(state == STATUS_LOADED_ALL && shownHamsters.isEmpty());
            view.selectItem(selectedItem);
        }
    }

    @Override
    public void onItemClicked(int index) {
        HamsterListView view = view();
        if (view != null && index >= 0 && index < shownHamsters.size()) {
            view.openDetails(shownHamsters.get(index));
            view.selectItem(index);
            selectedItem = index;
        }
    }

    @Override
    public void onRefresh() {
        HamsterListView view = view();
        if (state != STATUS_LOADING) {
            state = STATUS_REFRESHING;
            model.getHamsters(new HamsterLoadingCallback());
        }
        if (view != null) {
            view.setRefreshing(state == STATUS_REFRESHING);
        }
    }

    @Override
    public void searchRequested(String query) {
        if (TextUtils.equals(this.searchQuery, query)) {
            return;
        }

        this.searchQuery = query;
        if (searchingAsyncTask != null && !searchingAsyncTask.isCancelled()) {
            searchingAsyncTask.cancel(true);
        }
        searchingAsyncTask = new SearchingAsyncTask(allHamsters, query);
        searchingAsyncTask.execute();
        state = STATUS_LOADING;
        shownHamsters.clear();
        updateView();
    }

    @Override
    public void searchOpened() {
        isSearching = true;
    }

    @Override
    public void searchClosed() {
        isSearching = false;
        searchQuery = "";
        if (searchingAsyncTask != null) {
            searchingAsyncTask.cancel(true);
        }
        shownHamsters.clear();
        shownHamsters.addAll(allHamsters);
        updateView();
    }

    @Override
    public boolean isSearchOpened() {
        return isSearching;
    }

    @Override
    public String getSearchQuery() {
        return searchQuery;
    }

    private void initialize() {
        state = STATUS_LOADING;
        model.getHamsters(new HamsterLoadingCallback());
    }

    private class HamsterLoadingCallback implements LoadingCallback<List<Hamster>> {

        @Override
        public void onSuccess(List<Hamster> hamsters) {
            state = STATUS_LOADED_ALL;
            DefaultHamsterListPresenter.this.allHamsters = hamsters;
            if (isSearching && searchingAsyncTask != null && searchingAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
                searchingAsyncTask.cancel(true);
                DefaultHamsterListPresenter.this.shownHamsters.clear();
                searchingAsyncTask = new SearchingAsyncTask(hamsters, searchQuery);
                searchingAsyncTask.execute();
                state = STATUS_LOADING;
            } else {
                DefaultHamsterListPresenter.this.shownHamsters.addAll(hamsters);
            }
            updateView();
        }

        @Override
        public void onError(Throwable e) {
            state = STATUS_READY;
            updateView();
        }
    }

    private class SearchingAsyncTask extends AsyncTask<Void, Void, List<Hamster>> {
        private List<Hamster> sourceHamsters;
        private String query;

        public SearchingAsyncTask(List<Hamster> sourceHamsters, String query) {
            this.sourceHamsters = new ArrayList<>(sourceHamsters);
            this.query = query.trim().replaceAll("\\s+", " ").intern();
        }

        @Override
        protected List<Hamster> doInBackground(Void... voids) {
            if (TextUtils.isEmpty(query.trim().replaceAll("\\s+", ""))) {
                return sourceHamsters;
            }

            String[] words = query.split(" ");
            List<Hamster> results = new ArrayList<>();
            for (int i = 0; i < sourceHamsters.size() && !isCancelled(); i++) {
                boolean isResult = false;
                String[] hamsterWords = sourceHamsters.get(i).getName().trim()
                        .replaceAll("\\s+", " ").split(" ");
                for (int j = 0; j < words.length && !isResult; j++) {
                    for (int k = 0; k < hamsterWords.length && !isResult; k++) {
                        if (words[j].equalsIgnoreCase(hamsterWords[k])) {
                            isResult = true;
                        }
                    }
                }
                if (isResult) {
                    results.add(sourceHamsters.get(i));
                }
            }
            return results;
        }

        @Override
        protected void onPostExecute(List<Hamster> hamsters) {
            if (!isCancelled()) {
                DefaultHamsterListPresenter.this.shownHamsters = hamsters;
                state = STATUS_LOADED_ALL;
                updateView();
            }
        }
    }
}
