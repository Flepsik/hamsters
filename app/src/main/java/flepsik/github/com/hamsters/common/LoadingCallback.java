package flepsik.github.com.hamsters.common;

public interface LoadingCallback<Item> {
    void onSuccess(Item item);

    void onError(Throwable e);
}
