package flepsik.github.com.hamsters.screens.hamster_list;

import java.util.List;

import flepsik.github.com.hamsters.common.mvp.BindView;
import flepsik.github.com.hamsters.models.Hamster;

public interface HamsterListView extends BindView {
    void setRefreshing(boolean isRefreshing);

    void setProgress(boolean inProgress);

    void showPlaceholder(boolean shouldShow);

    void openDetails(Hamster hamster);

    void showHamsters(List<Hamster> hamsterList);

    void selectItem(int index);
}
