package flepsik.github.com.hamsters.screens.hamster_list.impl;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.hamsters.R;
import flepsik.github.com.hamsters.models.Hamster;

public class HamsterListAdapter extends RecyclerView.Adapter<HamsterListAdapter.ViewHolder> {
    private List<HamsterItem> items = new ArrayList<>();
    private int lastSelectedItemIndex = -1;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.view_hamster_item, parent, false
        );
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Hamster> hamsters) {
        items.clear();
        for (Hamster hamster : hamsters) {
            items.add(new HamsterItem(hamster));
        }
        notifyDataSetChanged();
    }

    public void setSelectedIndex(int index) {
        if (index < 0 || index > items.size()) {
            return;
        }

        if (lastSelectedItemIndex != -1) {
            items.get(lastSelectedItemIndex).isSelected = false;
            notifyItemChanged(lastSelectedItemIndex);
        }
        items.get(index).isSelected = true;
        notifyItemChanged(index);
        lastSelectedItemIndex = index;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView description;
        View rootView;

        ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
            rootView = itemView.findViewById(R.id.root_view);
        }

        void bind(HamsterItem hamsterItem) {
            if (hamsterItem.hamster.isImportant()) {
                title.setText(title.getContext()
                        .getString(R.string.important, hamsterItem.hamster.getName()));
            } else {
                title.setText(hamsterItem.hamster.getName());
            }
            description.setText(hamsterItem.hamster.getDescription());
            rootView.setSelected(hamsterItem.isSelected);
        }
    }

    private static class HamsterItem {
        Hamster hamster;
        boolean isSelected;

        public HamsterItem(Hamster hamster) {
            this.hamster = hamster;
        }
    }
}
