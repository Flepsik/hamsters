package flepsik.github.com.hamsters.common.mvp;

import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;

public abstract class BasePresenter<View extends BindView> implements Presenter<View> {
    @IntDef({STATUS_NONE, STATUS_LOADING, STATUS_READY, STATUS_LOADED_ALL, STATUS_REFRESHING})
    @Retention(RetentionPolicy.SOURCE)
    protected  @interface LoadingState {
    }
    protected static final int STATUS_NONE = 0;
    protected static final int STATUS_LOADING = 1;
    protected static final int STATUS_READY = 2;
    protected static final int STATUS_LOADED_ALL = 3;
    protected static final int STATUS_REFRESHING = 4;

    private WeakReference<View> view;

    @Override
    public void onCreate(Bundle savedInstanceState) {

    }

    @Override
    public void updateView() {

    }

    @Override
    public void bindView(@NonNull View view) {
        this.view = new WeakReference<>(view);
        updateView();
    }

    @Override
    public void unbindView() {
        this.view = null;
    }


    @Override
    public void saveState(Bundle outState) {

    }

    @Override
    public void onDestroy() {

    }

    @Nullable
    protected View view() {
        if (view == null) {
            return null;
        } else {
            return view.get();
        }
    }
}

