package flepsik.github.com.hamsters.screens.hamster_list.impl;

import android.os.Handler;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import flepsik.github.com.hamsters.App;
import flepsik.github.com.hamsters.common.LoadingCallback;
import flepsik.github.com.hamsters.common.NetworkState;
import flepsik.github.com.hamsters.models.Hamster;
import flepsik.github.com.hamsters.repository.database.DatabaseRepository;
import flepsik.github.com.hamsters.repository.network.NetworkRepository;
import flepsik.github.com.hamsters.screens.hamster_list.HamsterListModel;

public class DefaultHamsterListModel implements HamsterListModel {
    private final NetworkRepository networkRepository;
    private final DatabaseRepository databaseRepository;

    public DefaultHamsterListModel(NetworkRepository networkRepository, DatabaseRepository databaseRepository) {
        this.networkRepository = networkRepository;
        this.databaseRepository = databaseRepository;
    }

    @Override
    public void getHamsters(LoadingCallback<List<Hamster>> callback) {
        if (App.sNetworkState.status != NetworkState.NOT_CONNECTED) {
            networkRepository.loadHamsters(new LoadingCallback<List<Hamster>>() {
                @Override
                public void onSuccess(List<Hamster> hamsters) {
                    databaseRepository.replaceHamsters(hamsters);
                    notifySuccess(callback, hamsters);
                }

                @Override
                public void onError(Throwable e) {
                    databaseRepository.getHamsters(callback);
                }
            });
        } else {
            databaseRepository.getHamsters(callback);
        }
    }

    private void notifySuccess(LoadingCallback<List<Hamster>> callback, List<Hamster> items) {
        if (callback != null) {
            Handler handler = new Handler();
            new Thread() {
                @Override
                public void run() {
                    items.sort((hamster, t1) -> {
                        if (hamster.isImportant() == t1.isImportant()) {
                            return 0;
                        }

                        return hamster.isImportant() ? -1 : 1;
                    });
                    handler.post(() -> callback.onSuccess(items));
                }
            }.run();
        }
    }
}
